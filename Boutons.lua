
function Boutton(text, func, func_param, width, height)
    return {
        width = width or 100,
        height = height or 50,
        func = func or function() print("Ce bouton n’a aucune fonction attachée") end,
        func_param = func_param,
        text = text or "Ce bouton n’a pas de text",
        bouton_x = 0,
        bouton_y = 0,
        text_x = 0,
        text_y = 0,

        checkPressed = function(self, mouse_x, mouse_y)
            if (mouse_x >= self.bouton_x) and (mouse_x <= self.bouton_x + self.width) then
                if (mouse_y >= self.bouton_y) and (mouse_y <= self.bouton_y + self.height) then
                    if self.func_param then
                        self.func(self.func_param)
                    else
                        self.func()
                    end
                end
            end
        end,

        draw = function(self, bouton_x, bouton_y, text_x, text_y)
            self.bouton_x = bouton_x or self.bouton_x
            self.bouton_y = bouton_y or self.bouton_y

            if text_x then
                self.text_x = text_x + self.bouton_x
            else
                self.text_x = self.bouton_x
            end

            if text_y then
                self.text_y = text_y + self.bouton_y
            else
                self.text_y = text_y + self.bouton_y
            end
            love.graphics.setColor(0,0,0,1)
            love.graphics.rectangle("fill",self.bouton_x, self.bouton_y, self.width, self.height)

            love.graphics.setColor(1,1,1,1)
            love.graphics.print(self.text, self.text_x, self.text_y)


        end
    }

end

return Boutton