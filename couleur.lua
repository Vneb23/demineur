
t = {
    nombre = function(x,y,mines)
        love.graphics.setColor(1,1,1,1)
        if mines[x][y] == 1 then
            love.graphics.setColor(0.4/100,0,98.4/100,1)
        end
        if mines[x][y] == 2 then
            love.graphics.setColor(2.4/100,49.4/100,0.8/100,1)
        end
        if mines[x][y] == 3 then
            love.graphics.setColor(96.9/100,0.8/100,0,1)
        end
        if mines[x][y] == 4 then
            love.graphics.setColor(0.4/100,0,49.8/100,1)
        end
        if mines[x][y] == 5 then
            love.graphics.setColor(49.4/100,0.8/100,0,1)
        end
        if mines[x][y] == 6 then
            love.graphics.setColor(0/100,51.4/100,49.4/100,1)
        end
        if mines[x][y] == 7 then
            love.graphics.setColor(0,0,0,1)
        end
        if mines[x][y] == 8 then
            love.graphics.setColor(50.2/100,50.2/100,50.2/100,1)
        end
        if mines[x][y] == -1 then
            love.graphics.setColor(0,0,0,1)
        end
        if mines[x][y] == 0 then
            love.graphics.setColor(0.7,0.7,0.7,1)
        end
    end,
}

return t