local couleur = require "couleur"

flag = love.graphics.newImage("icon/flag.png")
l = love.graphics.getWidth(flag)
l = l / 9

t = {
    cadrillage = function(taille_case, nb_lignes, nb_colonnes,taille_espace)
        love.graphics.setColor(0,0,0,1)
        for k = 1, (nb_lignes-1) do
            love.graphics.line(taille_case/2,(k * (taille_case))+ (taille_case/2),(taille_case/2) + (taille_case * nb_colonnes),(k * (taille_case))+ (taille_case/2))
        end
        for k = 1, (nb_colonnes-1) do
            love.graphics.line((k * (taille_case)) + (taille_case/2), 0.5 * taille_case, (k * (taille_case)) + (taille_case/2), (nb_lignes+0.5)*taille_case)
        end
    end,
    fps = function()
        love.graphics.setColor(0,0,0,1)
        love.graphics.printf(
            "FPS : "..love.timer.getFPS(),
            love.graphics.newFont(16),
            10,
            love.graphics.getHeight() - 30,
            love.graphics.getWidth())
    end,
    plateau = function(mines,visible,nb_colonnes,nb_lignes,taille_case,font,taille_espace)
        for x= 1,nb_colonnes do
            for y = 1,nb_lignes do
                if visible[x][y] then
                    couleur.nombre(x,y,mines)
                    love.graphics.printf(mines[x][y],font,(x * (taille_case))-2, (y * (taille_case))- taille_case/4,love.graphics.getWidth())
                else
                    love.graphics.setColor(0.5,0.5,0.5,1)
                    love.graphics.rectangle("fill",((x-1) * taille_case) + (0.5 * taille_case) + 1,((y-1) * taille_case) + (0.5 * taille_case),taille_case - 1,taille_case- 1)
                end
            end
        end
    end,
    drapeau = function(drapeau,nb_colonnes,nb_lignes)
        for i = 1, nb_colonnes do
            for j = 1, nb_lignes do
                if drapeau[i][j] then
                    love.graphics.draw(flag,((i-1) * taille_case) + (0.7 * taille_case), ((j-1) * taille_case) + (0.5 * taille_case), 0,taille_case/l)
                end
            end
        end
    end,
    text = function(mines_restantes,t)
        love.graphics.setColor(0,0,0,1)
        love.graphics.printf(
            "Il reste "..mines_restantes.." mines. Vous jouez depuis "..t.." secondes",
            love.graphics.newFont(16),
            love.graphics.getWidth() / 2,
            love.graphics.getHeight() - 30,
            love.graphics.getWidth())
    end
}

return t