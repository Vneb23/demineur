local pile = require "pile"


function acceptable(pos,nb_colonnes,nb_lignes)
    if pos[1] < 1 or pos[2] < 1 then
        return false
    end
    if pos[1] > nb_colonnes then
        return false
    end
    if pos[2] > nb_lignes then
        return false
    end
    return true
end


function voisins(x,y,nb_colonnes,nb_lignes)
    vois = {}
    candidats = {{x-1,y-1},{x-1,y},{x-1,y+1},{x,y+1},{x+1,y+1},{x+1,y},{x+1,y-1},{x,y-1}}
    indice = 1
    for k=1,8 do
        if acceptable(candidats[k],nb_colonnes,nb_lignes) then
            vois[indice] = candidats[k]
            indice = indice + 1
        end
    end
    return vois
end

function empiler_si(p,v,visible,mines)
    for k = 1,#v do
        if not visible[v[k][1]][v[k][2]] then
            if mines[v[k][1]][v[k][2]] == 0 then
                pile.empiler(p,v[k])
            else
                visible[v[k][1]][v[k][2]] = "drapeau"
            end
        end
    end
end

function reveal(i,j,visible,mines,nb_colonnes,nb_lignes)
    visible[i][j] = true
    if mines[i][j] == 0 then
        p = pile.creer_pile()
        v = voisins(i,j,nb_colonnes,nb_lignes)
        empiler_si(p,v,visible,mines)
        while not pile.est_vide(p) do
            e = pile.depiler(p)
            visible[e[1]][e[2]] = true
            v = voisins(e[1],e[2],nb_colonnes,nb_lignes)
            empiler_si(p,v,visible,mines)
        end
    end
end


t = {
    gener_grille = function(nb_colonnes,nb_lignes,nb_mines)
        math.randomseed(os.time())
        mines = {}
        visible = {}
        drapeau = {}
        for x = 1, nb_colonnes do
            mines[x] = {}
            visible[x] = {}
            drapeau[x] = {}
            for y = 1, nb_lignes do
                mines[x][y] = 0
                visible[x][y] = false
                drapeau[x][y] = false
            end
        end
        for k = 1, nb_mines do
                x = math.random(nb_colonnes)
                y = math.random(nb_lignes)
                while mines[x][y] == -1 do
                    x = math.random(nb_colonnes)
                    y = math.random(nb_lignes)
                end
                mines[x][y] = -1
                vois = voisins(x,y,nb_colonnes,nb_lignes)
                for k =1,#vois do
                    v_x = vois[k][1]
                    v_y = vois[k][2]
                    if mines[v_x][v_y] ~= -1 then
                        mines[v_x][v_y] = mines[v_x][v_y] + 1
                    end
                end
        end
        return mines,visible,drapeau
    end,
    reveal = function(i,j,visible,mines,nb_colonnes,nb_lignes)
        visible[i][j] = true
        if mines[i][j] == 0 then
            p = pile.creer_pile()
            v = voisins(i,j,nb_colonnes,nb_lignes)
            empiler_si(p,v,visible,mines)
            while not pile.est_vide(p) do
                e = pile.depiler(p)
                visible[e[1]][e[2]] = true
                v = voisins(e[1],e[2],nb_colonnes,nb_lignes)
                empiler_si(p,v,visible,mines)
            end
        end
    end,
    gagned = function(visible,grille)
        for i = 1, #visible do
            for j = 1, #visible[1] do
                if mines[i][j] ~= -1 and not visible[i][j] then
                    return false
                end
            end
        end
        return true
    end,
    bouge_mine = function(i,j,mines,nb_colonnes,nb_lignes)
        x = math.random(nb_colonnes)
        y = math.random(nb_lignes)
        while mines[x][y] == -1 do
            x = math.random(nb_colonnes)
            y = math.random(nb_lignes)
        end
        mines[i][j] = 0
        mines[x][y] = -1
        v_init = voisins(i,j,nb_colonnes,nb_lignes)
        v_new = voisins(x,y,nb_colonnes, nb_lignes)
        for k = 1,#v_init do
            if mines[v_init[k][1]][v_init[k][2]] ~= -1 then
                mines[v_init[k][1]][v_init[k][2]] = mines[v_init[k][1]][v_init[k][2]] - 1
            else
                mines[i][j] = mines[i][j] + 1
            end
        end
        for k = 1,#v_new do
            if mines[v_new[k][1]][v_new[k][2]] ~= -1 then
                mines[v_new[k][1]][v_new[k][2]] = mines[v_new[k][1]][v_new[k][2]] + 1
            end
        end
    end,
    valide = function(i,j,nb_colonnes,nb_lignes)
        if i < 1 or i > nb_colonnes or j < 1 or j > nb_lignes then
            return false
        else
            return true
        end
    end,
    check_molette = function(i,j,nb_colonnes, nb_lignes,visible,drapeau,mines)
        v = voisins(i,j,nb_colonnes,nb_lignes)
        s = 0
        if not visible[i][j] then
            return false
        end
        for k =1,#v do
            if drapeau[v[k][1]][v[k][2]] then
                s = s + 1
            end
        end
        if s == mines[i][j] then
            for k = 1,#v do
                if not drapeau[v[k][1]][v[k][2]] and mines[v[k][1]][v[k][2]] == -1 then
                    return "boom"
                end
            end
            return true
        else
            return false
        end
    end,
    reveal_molette = function(i,j,nb_colonnes,nb_lignes,visible,drapeau)
        local v = voisins(i,j,nb_colonnes,nb_lignes)
        for k = 1,#v do
            if not drapeau[v[k][1]][v[k][2]] then
                reveal(v[k][1],v[k][2],visible,mines,nb_colonnes,nb_lignes)
            end
        end
    end
}


return t