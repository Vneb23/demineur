local love = require("love")
local grille = require "Grille"
local dessin = require "dessin"
local pile = require "pile"
local fsouris = require "souris"
local click = require "click"
local bouton = require "Boutons"
local math = require "math"
local settings = require "settings"

local souris = {
    x = 0,
    y = 0,
    down = false,
    down_droit = false,
    down_molette = false
}

local param = settings.lire()


print(param.nb_colonnes)
print(param.nb_lignes)
print(param.nb_mines)
local game = {
    difficulte = "facile",
    etat = {
        menu = true,
        pause = false,
        running = false,
        victoire = "jeu"
    },
    mines_restantes = param.nb_mines,
    time = 0,
    t_final = 0
}

local boutons = {
    menu = {},
}


param.font =love.graphics.setNewFont(param.taille_case/2)

jouer = function()
    game.etat.menu = false
    game.mines_restantes = param.nb_mines
end

rejouer = function()
    game.etat.running = false
    mines,visible,drapeau = grille.gener_grille(param.nb_colonnes, param.nb_lignes, param.nb_mines)
    game.etat.victoire = "jeu"
    game.mines_restantes = param.nb_mines
end

function love.load()
    love.graphics.setBackgroundColor (0.7,0.7,0.7,1)
    mines,visible,drapeau = grille.gener_grille(param.nb_colonnes, param.nb_lignes, param.nb_mines)
    love.window.setMode((param.nb_colonnes+1) * (param.taille_case +1),((param.nb_lignes+1) * (param.taille_case + 1)))
    boutons.menu.jouer = bouton("Jouer", jouer, nil, 4 * param.taille_case, 2 * param.taille_case)
    boutons.menu.parametres = bouton("Paramètres", nil, nil, 4 * param.taille_case, 2 * param.taille_case)
    boutons.menu.quitter = bouton("Quitter", love.event.quit, nil, 4 * param.taille_case, 2 * param.taille_case)
    boutons.rejouer = bouton("Rejouer", rejouer, nil, 4 * param.taille_case, 2 * param.taille_case)
end

function love.update(dt)
    nb_colonnes = param.nb_colonnes
    nb_lignes = param.nb_lignes
    taille_case = param.taille_case
    x,y = love.mouse.getPosition()
    if game.etat.menu then
        if fsouris.new_click_gauche(souris) then
            boutons.menu.quitter:checkPressed(x,y)
            boutons.menu.jouer:checkPressed(x,y)
        end
        if fsouris.fin_click_gauche(souris) then
            souris.down = false
        end
    elseif game.etat.victoire == "perdu" or game.etat.victoire == "gagné" then
        if fsouris.new_click_gauche(souris) then
            boutons.rejouer:checkPressed(x,y)
            boutons.menu.quitter:checkPressed(x,y)
        end
        if fsouris.fin_click_gauche(souris) then
            souris.down = false
        end
    else
        click.click_gauche(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        click.click_droit(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        click.click_molette(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        if grille.gagned(visible,grille) and game.etat.running then
            game.etat.victoire = "gagné"
            game.etat.running = false
            game.t_final = love.timer.getTime()
        end
    end
end

function love.draw()
    nb_colonnes = param.nb_colonnes
    nb_lignes = param.nb_lignes
    taille_case = param.taille_case
    font = param.font
    if game.etat.menu then
        boutons.menu.jouer:draw(10, 20, 10, 20)
        boutons.menu.parametres:draw(200, 20, 10, 20)
        boutons.menu.quitter:draw(400, 20, 10, 20)
    else
        if game.etat.victoire == "jeu" then
            dessin.plateau(mines,visible,nb_colonnes,nb_lignes,taille_case,font,taille_espace)
            dessin.cadrillage(taille_case, nb_lignes, nb_colonnes,1)
            dessin.drapeau(drapeau,nb_colonnes,nb_lignes)
            if game.etat.running then
                dessin.text(game.mines_restantes,math.floor(love.timer.getTime() - game.time))
            end
        elseif game.etat.victoire == "gagné" then
            love.graphics.print("Bravo ! Vous avez fini la grille en "..(math.floor((game.t_final - game.time)*100)/100).." secondes" )
            boutons.rejouer:draw(10,200,10,20)
            boutons.menu.quitter:draw(10,400,10,20)

        else
            love.graphics.print("Perdud lol")
            boutons.rejouer:draw(10,200,10,20)
            boutons.menu.quitter:draw(10,400,10,20)
        end
    end
    dessin.fps()
end
