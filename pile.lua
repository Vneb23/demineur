t = {
    creer_pile = function()
        return {0}
    end,
    est_vide = function(p)
        return (p[1] == 0)
    end,
    empiler = function(p,e)
        p[1] = p[1] + 1
        p[p[1]+1] = e
    end,
    depiler = function(p)
        e = p[p[1]+1]
        p[1] = p[1] - 1
        return e
    end
}

return t