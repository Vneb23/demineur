grille = require "Grille"
fsouris = require "souris"

t = {
    click_gauche = function(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        if fsouris.new_click_gauche(souris) then
            souris.x, souris.y = love.mouse.getPosition()
            i = math.floor((souris.x - taille_case / 2) / taille_case) + 1
            j = math.floor((souris.y - taille_case / 2) / taille_case) + 1
            if grille.valide(i,j,nb_colonnes, nb_lignes) then
                if not drapeau[i][j] then
                    if game.etat.running then
                        if mines[i][j] == -1 then
                            game.etat.victoire = "perdu"
                            game.etat.running = false
                        else
                            grille.reveal(i,j,visible,mines,nb_colonnes,nb_lignes)
                        end
                    else
                        if mines[i][j] == -1 then
                                grille.bouge_mine(i,j,mines,nb_colonnes,nb_lignes)
                            grille.reveal(i,j,visible,mines,nb_colonnes,nb_lignes)
                            game.etat.running = true
                        else
                            grille.reveal(i,j,visible,mines,nb_colonnes,nb_lignes)
                            game.etat.running = true
                        end
                        game.time = love.timer.getTime()
                    end
                end
            end
        end
        if fsouris.fin_click_gauche(souris) then
            souris.down = false
        end
    end,
    click_droit = function(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        if fsouris.new_click_droit(souris) then
            souris.x, souris.y = love.mouse.getPosition()
            i = math.floor((souris.x - taille_case / 2) / taille_case) + 1
            j = math.floor((souris.y - taille_case / 2) / taille_case) + 1
            if grille.valide(i,j,nb_colonnes,nb_lignes) then
                if drapeau[i][j] then
                    drapeau[i][j] = false
                    game.mines_restantes = game.mines_restantes + 1
                elseif not visible[i][j] then
                    drapeau[i][j] = true
                    game.mines_restantes = game.mines_restantes - 1
                end
            end
        end
        if fsouris.fin_click_droit(souris) then
            souris.down_droit = false
        end
    end,
    click_molette = function(souris,taille_case,mines,drapeau,visible,nb_colonnes,nb_lignes,game)
        if fsouris.new_click_molette(souris) then
            souris.x, souris.y = love.mouse.getPosition()
            i = math.floor((souris.x - taille_case / 2) / taille_case) + 1
            j = math.floor((souris.y - taille_case / 2) / taille_case) + 1
            if grille.valide(i,j,nb_colonnes, nb_lignes) then
                boom = grille.check_molette(i,j,nb_colonnes, nb_lignes,visible,drapeau,mines)
                if boom == "boom" then
                    game.etat.victoire = "perdu"
                elseif boom then
                    grille.reveal_molette(i,j,nb_colonnes,nb_lignes,visible,drapeau)
                end
            end
        end
        if fsouris.fin_click_molette(souris) then
            souris.down_molette = false
        end
    end
}

return t