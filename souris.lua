
t = {
    new_click_gauche = function(souris)
        if souris.down then
            return false
        else
            if love.mouse.isDown(1) then
                souris.down = true
                return true
            else
                return false
            end
        end
    end,
    fin_click_gauche = function(souris)
        if not souris.down then
            return false
        else
            if not love.mouse.isDown(1) then
                souris.down = false
                return true
            else
                return false
            end
        end
    end,
    new_click_droit = function(souris)
        if souris.down_droit then
            return false
        else
            if love.mouse.isDown(2) then
                souris.down_droit = true
                return true
            else
                return false
            end
        end
    end,
    fin_click_droit = function(souris)
        if not souris.down_droit then
            return false
        else
            if not love.mouse.isDown(2) then
                souris.down_droit = false
                return true
            else
                return false
            end
        end
    end,
    new_click_molette = function(souris)
        if souris.down_molette then
            return false
        else
            if love.mouse.isDown(3) then
                souris.down_molette = true
                return true
            else
                return false
            end
        end
    end,
    fin_click_molette = function(souris)
        if not souris.down_molette then
            return false
        else
            if not love.mouse.isDown(3) then
                souris.down_molette = false
                return true
            else
                return false
            end
        end
    end
}

return t

